$(document).ready(function(){
		$('.slider').advancedSlider({width:940, height:560, skin: 'curved-rounded-2', scrollbarSkin: 'scrollbar-3-light', horizontalSlices: 6, verticalSlices: 3, fadeNavigationButtons: true, 
									 thumbnailsType: 'navigation', thumbnailSync: true, visibleThumbnails: 4, fadeThumbnailArrows: true, thumbnailButtons: true, fadeThumbnailButtons: true, 
									 thumbnailScrollbar: false, fadeThumbnailScrollbar: true, hideThumbnailCaption: true, slideshow: false, slideshowControls: false, fadeSlideshowControls: false,                                     shadow: false, fadeNavigationArrows: false, fadeNavigationThumbnails: false, thumbnailButtons: false, navigationButtons: false, fadeThumbnailArrows: false,                                     thumbnailWidth: '116', thumbnailHeight:'69', navigationThumbnailsCenter: false, 
									 slideProperties:{
										 0: {captionHideEffect: 'slide', captionSize: 35}, 
										 1: {captionShowEffect: 'fade', captionPosition: 'custom', captionHeight: 160},
										 3: {captionPosition: 'top', captionSize: 40}, 
										 5: {captionHideEffect: 'slide', captionPosition: 'right', captionSize: 170}
									 }
		});
	});